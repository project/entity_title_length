Entity title length

INTRODUCTION
------------

The Entity Title Length allows your to change the length
of an entity (Ex: Node) title field.

REQUIREMENTS
------------

MySQL 5.0.3 or higher.

INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

2. Navigate to administer >> modules. Enable Entity title length.

3. Enjoy & Set your entity title length : http://example.com/admin/etl/config

CONFIGURATION
-------------

After the installation, there is some settings that you need
to edit to receive metrics by email

1. Navigate to administer >> configuration >> development
>> Entity title length configuration

2. Choose entity type & type title length

3. Save & increase title length
